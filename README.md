# Rad: R for Academics

Rad is a short course, intending to teach R to academics. It
doesn't assume familiarity with any other programming language.

You can access a published version of the course 
[here](https://bookdown.org/marius_mather/Rad/).

The content is free to share and modify under the 
[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) license.